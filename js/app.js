 const weather = {
    'apikey': '8ead93fc1c8a04ecb307396c4f09a5c9'
}

const fetchWeather = (city)=>{
    fetch('https://api.openweathermap.org/data/2.5/weather?q='+ city +'&units=metric&appid='+ weather.apikey +"&lang=es")
    .then((res)=>res.json())
    .then((data)=>{
        displayWeather(data)
    });
}
const displayWeather = (data)=>{
    const {name} = data;
    const {icon, description} = data.weather[0];
    const {temp, humidity} = data.main;
    const {speed} = data.wind;
    
    document.querySelector(".city").innerHTML = "Temperatura en " + name;
    document.querySelector(".icon").src = "https://openweathermap.org/img/wn/"+ icon + "@2x.png"
    document.querySelector(".description").innerText = description;
    document.querySelector(".temp").innerText = Math.round(temp) + "°C";
    document.querySelector(".humidity").innerText = "Humedad: "+ humidity + "%";
    document.querySelector(".wind").innerText = "Velocidad del viento: "+ speed + " km/h"
    document.querySelector(".weather").classList.remove("loading");
    document.body.style.backgroundImage = "url('https://source.unsplash.com/1600x900/?" + name + "')";
}

let geocode = {
    reverseGeoCode: function(latitude, longitude){
        var api_key = '23df4431168f49099878fa6e63ddb72e';

      
        var api_url = 'https://api.opencagedata.com/geocode/v1/json'
      
        var request_url = api_url
          + '?'
          + 'key=' + api_key
          + '&q=' + encodeURIComponent(latitude + ',' + longitude)
          + '&pretty=1'
          + '&no_annotations=1';
      
        // see full list of required and optional parameters:
        // https://opencagedata.com/api#forward
      
        var request = new XMLHttpRequest();
        request.open('GET', request_url, true);
      
        request.onload = function() {
          // see full list of possible response codes:
          // https://opencagedata.com/api#codes
      
          if (request.status === 200){ 
            // Success!
            var data = JSON.parse(request.responseText);
            fetchWeather(data.results[0].components.city);
      
          } else if (request.status <= 500){ 
            // We reached our target server, but it returned an error
                                 
            console.log("unable to geocode! Response code: " + request.status);
            var data = JSON.parse(request.responseText);
            console.log('error msg: ' + data.status.message);
          } else {
            console.log("server error");
          }
        };
      
        request.onerror = function() {
          // There was a connection error of some sort
          console.log("unable to connect to server");        
        };
      
        request.send();  // make the request
    },
    getLocation: function(){
        function Success(data){
            geocode.reverseGeoCode(data.coords.latitude, data.coords.longitude);  
        }
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(Success, console.error);
        }else{
            fetchWeather("Santiago");
        }
        
    }
}

const search = ()=>{
    fetchWeather(document.querySelector(".search-bar").value);
}

document.querySelector(".search button").addEventListener("click",()=>{
    search();
})

document.querySelector(".search-bar").addEventListener("keypress",(e)=>{
    let code = e.key;
    if(code == "Enter"){
        search();
    }
})

geocode.getLocation();